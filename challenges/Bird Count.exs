defmodule BirdCount do
  def today(list) do
    cond do
      [] == list -> nil
      true -> [head | others] = list
      head
    end
  end

  def increment_day_count(list) do
    cond do
      [] == list -> [1]
      true -> [head | tail] = list
      [head+1] ++ tail
    end
  end

  def has_day_without_birds?(list) do
    Enum.member?(list, 0)
  end

  def total(list) do

        cond do
           [] == list -> 0
          true -> [first | others ] = list
          first + total(others)
     end
  end


  def busy_days(list), do: busy_days(list, 0)
  defp busy_days([], count), do: count
  defp busy_days([h|t], count) when h >= 5, do: busy_days(t, count + 1)
  defp busy_days([_|t], count), do: busy_days(t, count)

end
