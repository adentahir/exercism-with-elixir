defmodule HighSchoolSweetheart do
  def first_letter(name) do
      name
      |> String.trim_leading()
      |> String.first()
  end

  def initial(name) do
      init = name
      |> first_letter()
      |> String.upcase()
      init <> "."
  end

  def initials(full_name) do
      [first, last] = full_name
        |> String.trim_leading()
        |> String.split(" ")
      initial(first)<>" "<>initial(last)
  end

  def pair(full_name1, full_name2) do


"     ******       ******\n   **      **   **      **\n **         ** **         **\n**            *            **\n**                         **\n**     "<>initials(full_name1) <> "  +  " <> initials(full_name2) <>"     **\n **                       **\n   **                   **\n     **               **\n       **           **\n         **       **\n           **   **\n             ***\n              *\n"

  end
end
