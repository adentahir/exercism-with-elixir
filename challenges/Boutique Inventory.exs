defmodule BoutiqueInventory do
  def sort_by_price(inventory) do
      Enum.sort_by(inventory, &(&1.price), :asc)

  end

  def with_missing_price(inventory) do
         inventory
    |> Enum.filter(&(&1.price == nil))
  end

  def update_names(inventory, old_word, new_word) do
     inventory
          |> Enum.map(fn item ->
          Map.replace(item, :name, String.replace(item.name, old_word, new_word))end)
  end

  def increase_quantity(item, count) do
        Map.update(item, :quantity_by_size, %{}, fn quantity_by_size ->
      quantity_by_size
      |> Enum.map(fn {size, quantity} -> {size, quantity + count} end)
      |> Enum.into(%{})
    end)
  end

  def total_quantity(item) do
        Enum.reduce(Map.get(item, :quantity_by_size), 0, fn {_size, quantity}, acc ->
        acc + quantity
  end)
end
end
