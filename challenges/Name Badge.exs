defmodule NameBadge do
  def print(id\\ nil, name, department\\ nil) do
  unless department == nil do
      if id == nil do
        "#{name} - #{String.upcase(department)}"
      else
        "[#{id}] - #{name} - #{String.upcase(department)}"
      end
  else
      if id == nil do
        "#{name} - OWNER"
      else
        "[#{id}] - #{name} - OWNER"
      end
  end

  end
end
