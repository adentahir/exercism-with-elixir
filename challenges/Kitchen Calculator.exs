defmodule KitchenCalculator do
  def get_volume(volume_pair) do
    {_ , num} = (volume_pair)
    num
  end

  def to_milliliter(volume_pair) do
    {unit, num} = (volume_pair)
    cond do
      {unit, num} == {:milliliter, num}  ->  {:milliliter, num}
      {unit, num} == {:fluid_ounce, num} -> {:milliliter, num * 30}
      {unit, num} == {:teaspoon , num}-> {:milliliter, num * 5}
      {unit, num} == {:tablespoon, num} -> {:milliliter, num * 15}
      {unit, num} == {:cup, num} -> {:milliliter, num * 240}
    end

  end

  def from_milliliter(volume_pair, unit) do
    {_unit_org, num} = (volume_pair)
    cond do
      {unit, num} == {:milliliter, num}  ->  {:milliliter, num}
      {unit, num} == {:fluid_ounce, num} -> {:fluid_ounce, num / 30}
      {unit, num} == {:teaspoon , num}-> {:teaspoon , num / 5}
      {unit, num} == {:tablespoon, num} -> {:tablespoon, num / 15}
      {unit, num} == {:cup, num} -> {:cup, num / 240}
    end
  end

  def convert(volume_pair, unit) do
    volume_pair = to_milliliter(volume_pair)
    from_milliliter(volume_pair, unit)

  end
end
