defmodule RPG do
  defmodule Character do
    defstruct health: 100, mana: 0
  end

  defmodule LoafOfBread do
    defstruct []
  end

  defmodule ManaPotion do
    defstruct strength: 10
  end

  defmodule Poison do
    defstruct []
  end

  defmodule EmptyBottle do
    defstruct []
  end

defprotocol Edible do
    def eat(item, character)
  end


  defimpl Edible, for: LoafOfBread do

    def eat(%RPG.LoafOfBread{}, %RPG.Character{health: h, mana: m}) do

      {nil, %RPG.Character{health: h+5, mana: m}}
    end
  end

  defimpl Edible, for: ManaPotion do

    def eat(%RPG.ManaPotion{strength: s}, %RPG.Character{health: h, mana: m}) do

      {%RPG.EmptyBottle{}, %RPG.Character{health: h, mana: m+s}}

    end

  end

  defimpl Edible, for: Poison do

    def eat(%RPG.Poison{}, %RPG.Character{mana: m}) do

      {%RPG.EmptyBottle{}, %RPG.Character{health: 0, mana: m}}
    end
  end

end
