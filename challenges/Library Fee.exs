defmodule LibraryFees do
  def datetime_from_string(string) do
     NaiveDateTime.from_iso8601!(string)
  end

  def before_noon?(%{hour: h}) do
      if h < 12 do
        true
      else
        false
      end
      end


  def return_date(checkout_datetime) do
        if before_noon?(checkout_datetime) do
         checkout_datetime
          |> NaiveDateTime.to_date()
          |> Date.add(28)
          else
          checkout_datetime
          |> NaiveDateTime.to_date()
          |> Date.add(29)
          end
  end

  def days_late(planned_return_date, actual_return_datetime) do
        diff = Date.diff(NaiveDateTime.to_date(actual_return_datetime), planned_return_date)
    if diff <= 0 do
    0
    else
    diff
    end
  end


  def monday?(datetime) do
       datetime
    |> NaiveDateTime.to_date()
    |> Date.day_of_week()
    |> Kernel.==(1)
  end

  def calculate_late_fee(checkout, return, rate) do
    return_datetime = datetime_from_string(return)
    days_late =
      checkout
      |> datetime_from_string()
      |> return_date()
      |> days_late(return_datetime)
    fee = rate * days_late
    if monday?(return_datetime), do: div(fee, 2), else: fee
  end
  end
