defmodule RemoteControlCar do
  @enforce_keys [:battery_percentage]
  defstruct [:battery_percentage, :distance_driven_in_meters, nickname: "none"]

 def new() do
     %RemoteControlCar{battery_percentage: 100, distance_driven_in_meters: 0}
 end

 def new(nickname) do
   %RemoteControlCar{battery_percentage: 100, distance_driven_in_meters: 0, nickname: nickname}
 end

 def display_distance(%__MODULE__{distance_driven_in_meters: distance, battery_percentage: _, nickname: _}) do
   "#{distance} meters"


 end

 def display_battery(%__MODULE__{distance_driven_in_meters: _, battery_percentage: battery, nickname: _}) do
    if battery != 0 do
       "Battery at #{battery}%"
    else "Battery empty"
    end
 end

 def drive(%__MODULE__{distance_driven_in_meters: distance, battery_percentage: battery, nickname: name}) do
   if battery != 0 do
     %__MODULE__{distance_driven_in_meters: distance + 20, battery_percentage: battery - 1, nickname: name}
   else
      %__MODULE__{distance_driven_in_meters: distance, battery_percentage: battery, nickname: name}
 end
 end
end
