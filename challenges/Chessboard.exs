defmodule Chessboard do
  def rank_range do
    1..8
  end

  def file_range do
    ?A..?H
  end

  def ranks do
    Enum.to_list(rank_range())
  end

  def files do
   file = Enum.map(file_range(), fn x -> <<x>> end)
    Enum.to_list(file)
  end
end
