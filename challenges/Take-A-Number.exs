defmodule TakeANumber do
  def start() do
    _pid = spawn(fn -> recursive_call(0) end)
  end

    defp recursive_call(state) do
      receive do
      {:report_state, sender_pid} -> send(sender_pid, state)
      recursive_call(state)

     {:take_a_number, sender_pid} ->
     new_state = state + 1
     send(sender_pid, new_state)
     recursive_call(new_state)

     :stop -> nil
     _->  recursive_call(state)

    end
end
end
