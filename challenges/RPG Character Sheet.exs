defmodule RPG.CharacterSheet do
  def welcome() do
    IO.puts "Welcome! Let's fill out your character sheet together."
    :ok
  end

  def ask_name() do
    name = IO.gets "What is your character's name?\n"
    String.trim(name)
  end

  def ask_class() do
    name = IO.gets "What is your character's class?\n"
    String.trim(name)
  end

  def ask_level() do
    lvl = IO.gets "What is your character's level?\n"
    String.to_integer(String.trim(lvl))
  end

  def run() do
    welcome()
        IO.inspect %{name: ask_name, class: ask_class, level: ask_level}, label: "Your character"
  end
end
